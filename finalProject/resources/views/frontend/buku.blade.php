<!-- resources/views/frontend/buku.blade.php -->

@extends('frontend.master')

@section('nav-items')
    <li class="nav-item">
        <a class="nav-link" href="/">Beranda</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/about">Tentang</a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="/buku">Buku</a>
    </li>
@endsection

@section('content')
    <div>

        <div class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10">
                        <!-- Form Pencarian -->
                        <form action="/search" method="GET" class="form-search-blog">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <select id="categories" class="custom-select bg-light" name="genre">
                                        <option value="">all genre</option>
                                        @forelse ($genre as $item)
                                            <option value="{{ $item->id }}" {{ request('genre') == $item->id ? 'selected' : '' }}>
                                                {{ $item->nama }}
                                            </option>
                                        @empty
                                            <option disabled>Tidak ada kategori</option>
                                        @endforelse
                                    </select>
                                </div>
                                <input type="text" class="form-control" placeholder="Judul or nama penulis " name="query" value="{{ request('query') }}">
                                
                                <!-- Search Button -->
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-2 text-sm-right">
                        <button class="btn btn-secondary">Filter <span class="mai-filter"></span></button>
                    </div>
                </div>

                <div class="row my-5">
                    <!-- Menampilkan hasil pencarian -->
                    @forelse ($books as $item)
                        <div class="col-6 col-md-4 col-lg-3 py-3 wow fadeInUp">
                            <a href="/buku/{{ $item->id }}">
                                <div class="card-blog">
                                    <div class="header">
                                        <div class="post-thumb d-flex justify-content-center align-items-center">
                                            <img src="{{ asset('cover/' . $item->poster) }}" class="" style="background-size: cover" alt="">
                                        </div>
                                    </div>
                                    <div class="body">
                                        <div>
                                            <span class="badge badge-primary p-1">{{ $item->genre->nama }}</span>
                                            <span class="badge badge-secondary p-1">{{ $item->penulis->nama }}</span>
                                        </div>
                                        <h5 class="post-title"><a href="/buku/{{ $item->id }}">{{ $item->judul }}</a></h5>
                                        <div class="post-date">Diunggah Pada <a href="#">{{ Str::limit($item->created_at, 16, '') }}</a></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @empty
                        <h4>Tidak ada buku yang ditemukan.</h4>
                    @endforelse
                </div>

                <!-- Pagination (jika diperlukan) -->
                <nav aria-label="Page Navigation">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item active" aria-current="page">
                            <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>

            </div>
        </div>
    </div>
@endsection
