## Final Project

## Kelompok 6

## Anggota Kelompok

- Anas Nasuha
- Riza Rihardina
- Tataq Distasianto

## Tema Project

Review Buku/Novel

## ERD

<img src="https://gitlab.com/anasnasuhaaa/books-review/-/raw/main/ERD.png?ref_type=heads">

## Link Video
 
Link Demo Aplikasi : [link demo](https://drive.google.com/file/d/1xx_-x_0J-7SUFrou9KNZ8tYWZ8-N9Mnh/view?usp=sharing)

Link Deploy : [link deploy](https://antara.project.booksreview.sanbercodeapp.com/)
